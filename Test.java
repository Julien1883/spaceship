package quiz.Test;

import quiz.spaceships.CommandCenter;
import quiz.spaceships.Spaceship;

import java.util.ArrayList;

public class Test {
    CommandCenter center = new CommandCenter();
    ArrayList<Spaceship> testList = new ArrayList<>();

    public static void main(String[] args) {
        System.out.println(0.44 * test());
    }

    public static int test() {
        Test test = new Test();
        int test1 = test.getShipByNameTest_ShipExist_resultIsHere();
        int test2 = test.getShipByNameTest_ShipExist_resultNull();
        int test3 = test.getMostPowerfulShip_ShipExist_resultIsHere();
        int test4 = test.getMostPowerfulShip_ShipNotExist_resultNull();
        int test5 = test.getMostPowerfulShip_ShipExist_resultsALot();
        int test6 = test.getAllShipsWithEnoughCargoSpace_ShipExist_resultList();
        int test7 = test.getAllShipsWithEnoughCargoSpace_ShipNotExist_resultNull();
        int test8 = test.getAllCivilianShips_ShipExist_resultIsHere();
        int test9 = test.getAllCivilianShips_ShipNotExist_resultNull();
        return test1 + test2 + test3 + test4 + test5 + test6 + test7 + test8 + test9;

    }

    public int getAllCivilianShips_ShipExist_resultIsHere() {
        testList.clear();
        testList.add(new Spaceship("Tom", 0, 23, 23));
        testList.add(new Spaceship("Tim", 2, 23, 23));
        testList.add(new Spaceship("Tam", 46, 23, 23));
        testList.add(new Spaceship("Tem", 0, 23, 23));
        ArrayList<Spaceship> ourShips = center.getAllCivilianShips(testList);
        if (ourShips.size() > 2) {
            return 0;
        }
        if (!ourShips.get(0).getName().equals("Tom")) {
            return 0;
        }
        if (!ourShips.get(1).getName().equals("Tem")) {
            return 0;
        }
        return 1;
    }

    public int getAllCivilianShips_ShipNotExist_resultNull() {
        testList.clear();
        testList.add(new Spaceship("Tom", 56, 23, 23));
        testList.add(new Spaceship("Tim", 45, 23, 23));
        testList.add(new Spaceship("Tam", 34, 23, 23));
        if (center.getAllCivilianShips(testList).size() == 0) {
            testList.clear();
            return 1;
        }
        return 0;
    }

    public int getAllShipsWithEnoughCargoSpace_ShipExist_resultList() {
        testList.add(new Spaceship("Tom", 23, 23, 23));
        testList.add(new Spaceship("Tim", 2, 23, 23));
        testList.add(new Spaceship("Tam", 46, 36, 23));
        testList.add(new Spaceship("Tem", 56, 45, 67));
        ArrayList<Spaceship> ourShips = center.getAllShipsWithEnoughCargoSpace(testList, 30);
        if (ourShips.size() > 2) {
            return 0;
        }
        if (ourShips.get(0) != testList.get(2)) {
            return 0;
        }
        if (ourShips.get(1) != testList.get(3)) {
            return 0;
        }
        testList.clear();
        return 1;
    }

    public int getAllShipsWithEnoughCargoSpace_ShipNotExist_resultNull() {
        testList.add(new Spaceship("Tom", 23, 23, 23));
        testList.add(new Spaceship("Tim", 2, 23, 23));
        testList.add(new Spaceship("Tam", 46, 36, 23));
        testList.add(new Spaceship("Tem", 56, 45, 67));
        if (center.getAllShipsWithEnoughCargoSpace(testList, 78).size() == 0) {
            testList.clear();
            return 1;
        }
        return 0;
    }

    public int getMostPowerfulShip_ShipExist_resultIsHere() {
        testList.add(new Spaceship("Tom", 23, 23, 23));
        testList.add(new Spaceship("Tim", 2, 23, 23));
        testList.add(new Spaceship("Tam", 46, 23, 23));
        if (center.getMostPowerfulShip(testList) == testList.get(2)) {
            testList.clear();
            return 1;
        }
        return 0;
    }

    public int getMostPowerfulShip_ShipNotExist_resultNull() {
        testList.add(new Spaceship("Tom", 0, 23, 23));
        testList.add(new Spaceship("Tim", 0, 23, 23));
        testList.add(new Spaceship("Tam", 0, 23, 23));
        if (center.getMostPowerfulShip(testList) == null) {
            testList.clear();
            return 1;
        }
        return 0;
    }

    public int getMostPowerfulShip_ShipExist_resultsALot() {
        testList.add(new Spaceship("Tom", 46, 23, 23));
        testList.add(new Spaceship("Tim", 46, 23, 23));
        testList.add(new Spaceship("Tam", 46, 23, 23));
        if (center.getMostPowerfulShip(testList).getName().equals("Tom")) {
            testList.clear();
            return 1;
        }
        return 0;
    }

    public int getShipByNameTest_ShipExist_resultIsHere() {
        testList.add(new Spaceship("Tom", 23, 23, 23));
        testList.add(new Spaceship("Tim", 2, 23, 23));
        testList.add(new Spaceship("Tam", 46, 23, 23));
        if (center.getShipByName(testList, "Tam") == testList.get(2)) {
            testList.clear();
            return 1;
        }
        return 0;
    }

    public int getShipByNameTest_ShipExist_resultNull() {
        testList.add(new Spaceship("Tom", 0, 23, 23));
        testList.add(new Spaceship("Tim", 0, 23, 23));
        testList.add(new Spaceship("Tam", 0, 23, 23));
        if (center.getShipByName(testList, "Bra") == null) {
            testList.clear();
            return 1;
        }
        return 0;
    }
}
