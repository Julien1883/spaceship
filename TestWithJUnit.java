package quiz.Test;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.Test;
import quiz.spaceships.CommandCenter;
import quiz.spaceships.Spaceship;

import java.util.ArrayList;

public class TestWithJUnit {
    CommandCenter center = new CommandCenter();
    ArrayList<Spaceship> testList = new ArrayList<>();
    boolean flag;

    public static void main(String[] args) {

    }

    @BeforeEach
    void variableDeclaration() {
        flag = false;
        testList.add(new Spaceship("Tom", 23, 20, 0));
        testList.add(new Spaceship("Tim", 2, 23, 0));
        testList.add(new Spaceship("Tam", 21, 36, 0));
        testList.add(new Spaceship("Tem", 23, 45, 0));
    }

    @AfterEach
    void cleaning() {
        testList.clear();
    }

    @Test
    @DisplayName("Мирных кораблей нет")
    void getAllCivilianShips_ShipNotExist_resultNull() {
        if (center.getAllCivilianShips(testList).size() == 0) {
            flag = !flag;
        }
        Assertions.assertTrue(flag);
    }

    @Test
    @DisplayName("Оп, появились")
    void getAllCivilianShips_ShipExist_resultIsHere() {
        testList.add(new Spaceship("ImPeaceful", 0, 45, 67));
        testList.add(new Spaceship("ImPeacefulToo", 0, 45, 67));
        if (center.getAllCivilianShips(testList).size() == 0) {
            flag = !flag;
        }
        ArrayList<Spaceship> ourShips = center.getAllCivilianShips(testList);
        if ((ourShips.get(0).getName().equals("ImPeaceful")) || (ourShips.get(1).getName().equals("ImPeacefulToo"))) {
            flag = !flag;
        }
        if (ourShips.size() > 2) {
            flag = !flag;

        }
        Assertions.assertTrue(flag);
    }

    @Test
    @DisplayName("Грузового трюма достаточно")
    void getAllShipsWithEnoughCargoSpace_ShipExist_resultList() {
        ArrayList<Spaceship> ourShips = center.getAllShipsWithEnoughCargoSpace(testList, 22);

        if ((ourShips.get(0) == testList.get(0)) || (ourShips.get(1) == testList.get(3))) {
            flag = !flag;
        }
        if (ourShips.size() > 2) {
            flag = !flag;
        }
        Assertions.assertTrue(flag);
    }

    @Test
    @DisplayName("Недостаточно")
    void getAllShipsWithEnoughCargoSpace_ShipNotExist_resultNull() {
        if (center.getAllShipsWithEnoughCargoSpace(testList, 78).size() == 0) {
            flag = !flag;
        }
        Assertions.assertTrue(flag);
    }

    @Test
    @DisplayName("Ищем корабль по имени")
    void getShipByNameTest_ShipExist_resultIsHere() {
        Assertions.assertEquals(center.getShipByName(testList, "Tam"), testList.get(2));
    }

    @Test
    @DisplayName("Не нашли... и правильно")
    void getShipByNameTest_ShipExist_resultNull() {
        Assertions.assertNull(center.getShipByName(testList, "Bra"));
    }

    @Test
    @DisplayName("Теперь ищем самого сильного(он тут есть и один)")
    void  getMostPowerfulShip_ShipExist_resultIsHere() {
        testList.add(new Spaceship("ImTheMostPowerful", 56, 20, 0));
        Assertions.assertEquals(center.getMostPowerfulShip(testList), testList.get(4));
    }

    @Test
    @DisplayName("Вот уже и не один")
    void  getMostPowerfulShip_ShipExist_resultsALot() {
        testList.add(new Spaceship("ImTheFirstMostPowerful", 56, 20, 0));
        testList.add(new Spaceship("ImTheSecond", 56, 20, 0));
        Assertions.assertEquals(center.getMostPowerfulShip(testList), testList.get(4));

    }

    @Test
    @DisplayName("А мы и мирными обойдемся")
    void getMostPowerfulShip_ShipNotExist_resultNull() {
        testList.get(0).setFirePower(0);
        testList.get(1).setFirePower(0);
        testList.get(2).setFirePower(0);
        testList.get(3).setFirePower(0);
        Assertions.assertNull(center.getMostPowerfulShip(testList) );
    }
}
