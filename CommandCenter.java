package quiz.spaceships;


import java.util.ArrayList;

// Вы проектируете интеллектуальную систему управления ангаром командного центра.
// Реализуйте интерфейс SpaceshipFleetManager для управления флотом кораблей.
// Используйте СУЩЕСТВУЮЩИЙ интерфейс и класс космического корабля (SpaceshipFleetManager и Spaceship).
public class CommandCenter implements SpaceshipFleetManager {

    @Override
    public Spaceship getMostPowerfulShip(ArrayList<Spaceship> ships) {
        int max = 0;
        Spaceship ourSpaceShip = null;
        for (int i = 0; i < ships.size(); i++) {
            if (ships.get(i).getFirePower() > max) {
                ourSpaceShip = ships.get(i);
                max = ourSpaceShip.getFirePower();
            }
        }
        return ourSpaceShip;
    }

    @Override
    public Spaceship getShipByName(ArrayList<Spaceship> ships, String name) {
        for (int i = 0; i < ships.size(); i++) {
            if (ships.get(i).getName().equals(name)) {
                return ships.get(i);
            }
        }
        return null;
    }

    @Override
    public ArrayList<Spaceship> getAllShipsWithEnoughCargoSpace(ArrayList<Spaceship> ships, Integer cargoSize) {
        ArrayList<Spaceship> ourShips = new ArrayList<>();
        for (int i = 0; i < ships.size(); i++) {
            if (ships.get(i).getCargoSpace() >= cargoSize) {
                ourShips.add(ships.get(i));
            }
        }
        return ourShips;
    }

    @Override
    public ArrayList<Spaceship> getAllCivilianShips(ArrayList<Spaceship> ships) {
        ArrayList<Spaceship> ourShips = new ArrayList<>();
        for (int i = 0; i < ships.size(); i++) {
            if (ships.get(i).getFirePower() == 0) {
                ourShips.add(ships.get(i));
            }
        }
        return ourShips;
    }
}
